Source: schleuder
Section: mail
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Georg Faerber <georg@debian.org>,
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               procps,
               rake,
               puma,
               ruby-rackup,
               ruby-activerecord,
               ruby-bcrypt,
               ruby-charlock-holmes,
               ruby-database-cleaner,
               ruby-factory-bot,
               ruby-gpgme,
               ruby-mail,
               ruby-rack-test,
               ruby-rspec,
               ruby-sinatra,
               ruby-sinatra-contrib,
               ruby-sqlite3,
               ruby-thor,
               ruby-typhoeus,
               thin,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/ruby-team/schleuder.git
Vcs-Browser: https://salsa.debian.org/ruby-team/schleuder
Homepage: https://schleuder.org/
Rules-Requires-Root: no

Package: schleuder
Architecture: all
Depends: adduser,
         cron | cron-daemon,
         default-mta | postfix | mail-transport-agent,
         rake,
         ruby,
         ruby-activerecord,
         ruby-bcrypt,
         ruby-charlock-holmes,
         ruby-gpgme,
         ruby-mail,
         puma,
         ruby-rackup,
         ruby-sinatra,
         ruby-sinatra-contrib,
         ruby-sqlite3,
         ruby-thor,
         ruby-typhoeus,
         thin,
         ${misc:Depends},
         ${shlibs:Depends},
Pre-Depends: ${misc:Pre-Depends},
Recommends: schleuder-cli,
Description: encrypting mailing list manager with remailing-capabilities
 Schleuder is an encrypting mailing list manager with remailing-capabilities.
 Subscribers can communicate encrypted (and pseudonymously) among themselves,
 receive emails from non-subscribers and send emails to non-subscribers via the
 list.
 .
 Schleuder aims to be robust, flexible, and internationalized. It also provides
 an API to be used with schleuder-cli and/or schleuder-web.
